'''
CSV parser module
'''
__all__ = ['parse_csv']


def parse_csv(filename, sep=','):
    '''Parse a single line string

    Args:
        filename: full path to file
        sep: separator

    Returns:
        Parsed file contents as a list of lists
    '''

    with open(filename, 'r') as stream:
        return [list(parse_line(line, sep=sep)) for line in stream]


def parse_line(line, sep):
    return (parse_item(item) for item in line.split(sep))


def parse_item(item):
    if item.startswith('"') and item.endswith('"'):
        return item[1:-1]
    try:
        return int(item)
    except ValueError:
        return float(item)

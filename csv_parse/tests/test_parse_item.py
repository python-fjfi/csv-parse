import pytest

from csv_parse.csv_parse import parse_item


def test_parse_string():
    item = '"text"'
    assert parse_item(item) == 'text'


def test_parse_float():
    item = '1.2'
    assert parse_item(item) == 1.2


def test_parse_int():
    item = '-2'
    assert parse_item(item) == -2


@pytest.mark.parametrize('item', ['text', '"x', '1.2.3', ''])
def test_exceptio_on_wrong_input(item):
    with pytest.raises(ValueError):
        parse_item(item)

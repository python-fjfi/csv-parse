import csv_parse


def test_parser_is_not_implemented(tmpdir):
    tmpfile = tmpdir.join('whatever_filename.csv')
    tmpfile.write('"A", 1, 2, 3.3')
    assert csv_parse.parse_csv(tmpfile.strpath) == [['A', 1, 2, 3.3]]
